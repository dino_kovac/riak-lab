require 'sinatra/base'
require 'sinatra/reloader'
require 'riak'
require 'slim'
require 'json'
require 'time'
require 'pry'

class RiakNews < Sinatra::Base

  @@client = Riak::Client.new(nodes: [ { host: '192.168.56.12', http_port: 10018 }, { host: '192.168.56.12', http_port: 10028 }, { host: '192.168.56.12', http_port: 10038 } ])
  @@news_bucket = @@client['news']
  @@images_bucket = @@client['images']
  @@comments_bucket = @@client['comments']
  @@latest_bucket = @@client['latest']
  @@latest_article = @@latest_bucket.get_or_new 'latest_article'
  @@latest_article.content_type = 'text/plain'
  @@latest_article.data = 'latest article'

  configure :development do
    register Sinatra::Reloader
  end

  def get_latest_news
    latest_link = @@latest_article.links.find { |l| l.tag == 'latest_article' }
    @@news_bucket.get(latest_link.key) if latest_link
  end

  def get_news(count)
    latest_news = get_latest_news

    news = []

    if latest_news && count > 0
      news << latest_news
      previous = get_previous_news(latest_news)
      count -= 1

      count.times do
        break unless previous
        news << previous
        previous = get_previous_news(previous)
      end
    end

    news
  end

  def get_previous_news(n)
    prev_link = n.links.find { |l| l.tag == 'previous' }
    @@news_bucket.get(prev_link.key) if prev_link
  end

  def get_comments(n)
    comments = []
    comment_link = n.links.find { |l| l.tag == 'last_comment' }
    comment = @@comments_bucket.get(comment_link.key) if comment_link
    while comment
      comments << comment
      comment_link = comment.links.find { |l| l.tag == 'previous' }
      if comment_link
        comment = @@comments_bucket.get(comment_link.key)
      else
        comment = nil
      end
    end

    comments.map do |c|
      {
        content: c.data['content'],
        date: Time.iso8601(c.data['date']).strftime("%-d.%-m.%Y. at %-H:%M")
      }
    end
  end

  def map_news(news)
    news.map do |n|
      image_link = n.links.find { |l| l.tag == 'image' }
      image_raw = @@images_bucket.get(image_link.key).raw_data
      {
        key: n.key,
        heading: n.data['heading'],
        content: n.data['content'],
        author: n.data['author'],
        date: Time.iso8601(n.data['date']).strftime("%-d.%-m.%Y. at %-H:%M"),
        image: "data:image/png;base64,#{Base64.encode64(image_raw)}",
        comments: get_comments(n)
      }
    end
  end

  get '/' do
    count = 10
    if params['count']
      count = params['count'].to_i
    end

    @news = get_news(count)
    @news = map_news(@news)

    slim :index
  end

  get '/add' do
    slim :add
  end

  post '/' do
    news = @@news_bucket.new
    news.data = { heading: params['news']['heading'], content: params['news']['content'], author: params['news']['author'], date: Time.now.utc.iso8601, comment_count: 0 }
    news.content_type = 'application/json'

    halt 400, 'No image found, it must be inside the project folder' unless File.file?(params['news']['file'])

    image = @@images_bucket.new
    image.content_type = 'image/png'
    image.raw_data = File.read(params['news']['file'])
    image.store

    news.links = [ Riak::Link.new('images', image.key, 'image') ]
    latest_news = get_latest_news
    if latest_news
      news.links << Riak::Link.new('news', latest_news.key, 'previous')
    end
    news.store

    @@latest_article.links = [ Riak::Link.new('news', news.key, 'latest_article') ]

    @@latest_article.store
    redirect '/'
  end

  post '/:key/comment' do
    halt 404, 'Not found' unless @@news_bucket.exist?(params['key'])
    news = @@news_bucket.get(params['key'])

    new_comment = @@comments_bucket.new
    new_comment.data = { content: params['content'], date: Time.now.utc.iso8601 }
    new_comment.content_type = 'application/json'

    comment_link = news.links.find { |l| l.tag == 'last_comment' }
    comment = @@comments_bucket.get(comment_link.key) if comment_link

    if comment
      new_comment.links << Riak::Link.new('comments', comment.key, 'previous')
      news.links.reject! { |l| l.tag == "last_comment" }
      news.data['comment_count'] += 1
    else
      news.data['comment_count'] = 1
    end

    new_comment.store

    news.links << Riak::Link.new('comments', new_comment.key, 'last_comment')

    news.store
    redirect '/'
  end

  get '/comments-desc' do
    results = Riak::MapReduce.new(@@client).add("news")
    .map('function(news) {
        return [{key: news.key, value: JSON.parse(news.values[0].data)["comment_count"]}];
      }')
    .reduce('function(values) {
      return values.sort(
        function(a, b) {
            return b["value"] - a["value"];
        }
      );
    }', :keep => true).run

    @news = results.map { |r| @@news_bucket.get(r['key']) }
    @news = map_news(@news)

    slim :index
  end

  get '/words' do

    @results = Riak::MapReduce.new(@@client).add("news")
    .map('function(news) {
        var text = JSON.parse(news.values[0].data)["heading"] + " " + JSON.parse(news.values[0].data)["content"];
        var words = text.toLowerCase().match(/\w*/g).filter(function(e){return e});
        return [{author: JSON.parse(news.values[0].data)["author"], value: words}];
      }')
    .reduce('function(values) {
        var m = {};
        for (var i in values) {
          if (values[i]["author"] in m) {
            m[values[i]["author"]] = m[values[i]["author"]].concat(values[i]["value"]);
          } else {
            m[values[i]["author"]] = values[i]["value"];
          }
        }
        var results = [];
        for (var key in m) {
          if (m.hasOwnProperty(key)) {
            results.push({author: key, value: m[key]});
          }
        }
        return results;
      }')
    .reduce('function(values) {
        for (var i in values) {
          words = values[i]["value"];
          word_counts = {};
          for (var j in words) {
            if (words[j] in word_counts) {
              word_counts[words[j]] += 1;
            } else {
              word_counts[words[j]] = 1;
            }
          }

          words_array = [];
          for (var key in word_counts) {
            if (word_counts.hasOwnProperty(key)) {
              words_array.push({word: key, count: word_counts[key]});
            }
          }

          words_array.sort(
            function(a, b) {
                return b["count"] - a["count"];
            }
          );
          values[i]["value"] = words_array.slice(0, 10).map(function(w) {
            return w["word"];
          });
        }

        return values;
      }', :keep => true).run

    slim :words
  end

  run! if app_file == $0

end
